#!/usr/bin/python
'''
 Copyright (c) 2019, 2020, Oracle and/or its affiliates. All rights reserved.

NAME:
    cca - Customer Control Access backend setup to access the domU by ops user.

FUNCTION:
    Provide CCA environment creation, deletion, modification on domU for Ops user SSH login remotely from oracle n/w

NOTE:
    None

History:
    kkviswan     11/03/2019 - to support CCA configuration on domU
'''

# Script to manage CCA environment
import argparse
import cca_ctrl_env
import json

def main():
    parser = setup_parser_options()
    args =  parser.parse_args()

    if args.command == "create":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=args.CHROOT_PATH)
        cca_env.create_environment(enable_sudo=args.esudo)
    elif args.command == "delete":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=args.CHROOT_PATH)
        cca_env.delete_environment()
    elif args.command == "add-user":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=args.CHROOT_PATH)
        (username, passwd) = cca_env.create_new_user(args.USERNAME)
        user_details = {'username': args.USERNAME, 'password': passwd, 'env-name': args.ENV_NAME}
        print json.dumps(user_details)
    elif args.command == "delete-user":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=args.CHROOT_PATH)
        cca_env.delete_user(args.USERNAME)
    elif args.command == "enable-user":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=None)
        cca_env.enable_user(args.USERNAME)
    elif args.command == "disable-user":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=None)
        cca_env.disable_user(args.USERNAME)
    elif args.command == "disconnect":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=None)
        cca_env.disconnect_user(args.USERNAME)
    elif args.command == "audit-user":
        cca_env = cca_ctrl_env.CcaEnvironment(env_name=args.ENV_NAME, chroot_dir=None)
        cca_env.audit_user_log(args.USERNAME, args.LOGTYPE, args.TIMEINTERVAL)

def setup_parser_options():
    parser = argparse.ArgumentParser(description='CCA environment configuration tool', add_help=True)
    subparsers = parser.add_subparsers(help='command', dest='command')

    list_parser = subparsers.add_parser('create', help='Create CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('CHROOT_PATH', help='CCA environment jail path', action="store")
    list_parser.add_argument('--with-sudo', action='store_true', help='Enable sudo execution in given environment', dest="esudo")

    list_parser = subparsers.add_parser('delete', help='Delete CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('CHROOT_PATH', help='CCA environment jail path', action="store")

    list_parser = subparsers.add_parser('add-user', help='Create Username in CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('CHROOT_PATH', help='CCA environment jail path', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Create Username in CCA environment')

    list_parser = subparsers.add_parser('delete-user', help='Delete Username in CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('CHROOT_PATH', help='CCA environment jail path', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Delete Username in CCA environment')

    list_parser = subparsers.add_parser('disable-user', help='Disable Username in CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Disable Username in CCA environment')

    list_parser = subparsers.add_parser('enable-user', help='Enable Username in CCA environment')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Enable Username in CCA environment')

    list_parser = subparsers.add_parser('disconnect', help='Disconnect all session and process related to Username')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Disconnect all session and process related to Username')

    list_parser = subparsers.add_parser('audit-user', help='Audit all user session and process related to Username')
    list_parser.add_argument('ENV_NAME', help='CCA environment name', action="store")
    list_parser.add_argument('USERNAME', action='store', help='Disconnect all session and process related to Username')
    list_parser.add_argument('LOGTYPE', action='store', help='login, commands or key-stokes')
    list_parser.add_argument('TIMEINTERVAL', action='store', help='User any of these now, recent, today, yesterday,  this-week,  week-ago,  this-month,  or  this-year')

    return parser

if __name__ == "__main__":
    main()
